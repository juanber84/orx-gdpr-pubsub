'use strict'

const axios = require('axios')
let env = process.env.FACTER_vm_env || 'staging'
let envParameters = require('./../env')

function requestCreation(token) {
    let headers = { "Content-Type": "application/json" }
    headers.Authorization = 'Orx ' + token
    return axios.create({
        baseURL: envParameters[env],
        timeout: 3000,
        headers
    })
}

module.exports = {
    revoke: {
        notify(data, token){
            const request = requestCreation(token)
            return request.put('/privacy/revoke', data)
        }
    },
    access: {
        notify(data, token) {
            const request = requestCreation(token)
            return request.put('/privacy/access', data)
        }
    }
}