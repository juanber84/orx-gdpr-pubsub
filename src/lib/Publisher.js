'use strict'

const PubSub = require('@google-cloud/pubsub')

class Publisher {
    constructor(projectId, credentials){
        this.pubsubclient = new PubSub({
            projectId,credentials
        })
    }

    createTopic(topicName){
        return this.pubsubclient.createTopic(topicName)
            .then(responses => {
                return responses[0]
            })
    }

    publish(topicName, data) {
        // Cast objet to string
        if (typeof data === 'object')
            data = JSON.stringify(data)
        // Cast data to buffer
        const dataBuffer = Buffer.from(data)
        // Send data
        return this.pubsubclient
            .topic(topicName)
            .publisher()
            .publish(dataBuffer)
            .then(response => {
                return response
            })
    }
}

module.exports = Publisher