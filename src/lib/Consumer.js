'use strict'

const PubSub = require('@google-cloud/pubsub')

class Consumer {
    constructor(projectId, credentials){
        this.pubsubclient = new PubSub({
            projectId, credentials
        })        
    }

    consume(topicName, subscriptionName, onMessage, onError){
        let topic = this.pubsubclient.topic(topicName)
        let subscription = topic.subscription(subscriptionName)
        subscription.on('message', onMessage)
        subscription.on('error', onError)
        return subscription
    }
}

module.exports = Consumer