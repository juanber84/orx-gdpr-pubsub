'use strict'

module.exports = {
    staging: 'https://core.s.orchextra.io/',
    quality: 'https://core.q.orchextra.io/',
    live: 'https://core.orchextra.io/'
}