'use strict'

const Consumer = require('./../src/lib/Consumer')

const credentials = require('./pass.1.json')
const projectId = credentials.project_id

const topicName = "gdprDev"
const subscriptionName = "gdprDevSub"

const consumer = new Consumer(projectId, credentials)

consumer.consume(topicName, subscriptionName, function(message) {
    
    console.log(message.data.toString())

    // Ack the message:
    message.ack();

    // This doesn't ack the message, but allows more messages to be retrieved
    // if your limit was hit or if you don't want to ack the message.
    message.nack();

})