'use strict'

const Publisher = require('./../src/lib/Publisher')

const credentials = require('./pass.1.json')
const projectId = credentials.project_id

const topicName = "gdprDev"
const data = {
    foo: 'bar'
}

const publisher = new Publisher(projectId, credentials)

;(async function () {
    try {
        await publisher.createTopic(topicName)
    } catch(err){
        if (err.code !== 6)
            console.log(err)
    }
    try {
        await publisher.publish(topicName, data)
    } catch(err){
        console.log(err)
    }
})();
